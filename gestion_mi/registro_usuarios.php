<?php

	include("include/validar_form.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="es-ES">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Registro de Usuarios </title>
	<link rel="stylesheet" href="main.css" type="text/css" media="screen" />
</head>
<body>
	<div class="wrapper">
		<div class="section">
			<?php if(!isset($status)): ?>
			<h1>Formulario de registro de Usuarios</h1>
			<form id="form1" action="registro_usuarios.php" method="post">

				<label for="email">E-mail (<span id="req-email" class="requisites <?php echo $email ?>">Un e-mail válido por favor</span>):</label>
				<input tabindex="4" name="email" id="email" type="text" class="text <?php echo $email ?>" value="<?php echo $emailValue ?>" />
				<label for="password1">Contraseña (<span id="req-password1" class="requisites <?php echo $password1 ?>">Mínimo 5 caracteres, máximo 12 caracteres, letras y números</span>):</label>
				<input tabindex="2" name="password1" id="password1" type="password" class="text <?php echo $password1 ?>" value="" />
				<label for="password2">Repetir Contraseña (<span id="req-password2" class="requisites <?php echo $password2 ?>">Debe ser igual a la anterior</span>):</label>
				<input tabindex="3" name="password2" id="password2" type="password" class="text <?php echo $password2 ?>" value="" />
				
				<div>
					<input tabindex="6" name="send" id="send" type="submit" class="submit" value="Registrarse" />
				</div>
			</form>


			<?php 
			extract($_GET);
			     if (@$status==1) {
			        echo "ingresar datos en bd";
			         # code...
			     }
			 ?>

				
			<?php endif; ?>
		</div>
	</div>
	<script type="text/javascript" src="jquery.js"></script> 
	<script type="text/javascript" src="main.js"></script>
</body>
</html>



